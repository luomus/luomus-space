package fi.luomus.spacereservation.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.utils.Utils;
import fi.luomus.spacereservation.model.Attraction;
import fi.luomus.spacereservation.model.OpeningTime;
import fi.luomus.spacereservation.model.OpeningTimes;

public class OpeningTimesTests {

	@Test
	public void test_weeks() {
		Attraction a1 = new Attraction(1, new LocalizedText());
		Attraction a2 = new Attraction(2, new LocalizedText());
		List<Attraction> attractions = Utils.list(a1, a2);

		OpeningTimes openingTimes = new OpeningTimes(Utils.list(
				new OpeningTime(1, a1, new DateValue(28, 12, 2021), 11, 00, 17, 00),
				new OpeningTime(7, a2, new DateValue(28, 12, 2021), 9, 00, 16, 30),
				new OpeningTime(2, a1, new DateValue(29, 12, 2021), 11, 00, 17, 00),
				new OpeningTime(3, a1, new DateValue(30, 12, 2021), 11, 00, 17, 00),
				new OpeningTime(4, a1, new DateValue(31, 12, 2021), 11, 00, 17, 00),
				new OpeningTime(5, a1, new DateValue(1, 1, 2022), 11, 00, 17, 00),
				new OpeningTime(8, a2, new DateValue(1, 1, 2022), 9, 00, 20, 00),
				new OpeningTime(6, a1, new DateValue(2, 1, 2022), 11, 00, 17, 00)
				), attractions);

		assertEquals("[" +
				"OpeningTime [attraction=1, date=28.12.2021, startHour=11, startMinutes=0, endHour=17, endMinutes=0, closed=false], " +
				"OpeningTime [attraction=2, date=28.12.2021, startHour=9, startMinutes=0, endHour=16, endMinutes=30, closed=false]]",
				openingTimes.getOpeningTimes(new DateValue(28, 12, 2021)).toString());

		assertEquals("["+
				"OpeningTime [attraction=1, date=27.12.2021, closed=true], " +
				"OpeningTime [attraction=2, date=27.12.2021, closed=true]]",
				openingTimes.getOpeningTimes(new DateValue(27, 12, 2021)).toString());

		assertEquals("[" +
				"OpeningTime [attraction=1, date=01.01.1900, closed=true], " +
				"OpeningTime [attraction=2, date=01.01.1900, closed=true]]",
				openingTimes.getOpeningTimes(new DateValue(1, 1, 1900)).toString());

		assertEquals("[" +
				"OpeningTime [attraction=1, date=01.01.1900, closed=true], " +
				"OpeningTime [attraction=1, date=02.01.1900, closed=true], " +
				"OpeningTime [attraction=1, date=03.01.1900, closed=true], " +
				"OpeningTime [attraction=2, date=01.01.1900, closed=true], " +
				"OpeningTime [attraction=2, date=02.01.1900, closed=true], " +
				"OpeningTime [attraction=2, date=03.01.1900, closed=true]]",
				openingTimes.getOpeningTimes(new DateValue(1, 1, 1900), new DateValue(3, 1, 1900)).toString());

		assertEquals("[" +
				"OpeningTime [attraction=1, date=27.02.2022, closed=true], " +
				"OpeningTime [attraction=1, date=28.02.2022, closed=true], " +
				"OpeningTime [attraction=1, date=01.03.2022, closed=true], " +
				"OpeningTime [attraction=1, date=02.03.2022, closed=true], " +
				"OpeningTime [attraction=2, date=27.02.2022, closed=true], " +
				"OpeningTime [attraction=2, date=28.02.2022, closed=true], " +
				"OpeningTime [attraction=2, date=01.03.2022, closed=true], " +
				"OpeningTime [attraction=2, date=02.03.2022, closed=true]]",
				openingTimes.getOpeningTimes(new DateValue(27, 2, 2022), new DateValue(2, 3, 2022)).toString()); // 28.2 is the last day of Feb

		assertEquals("[" +
				"OpeningTime [attraction=1, date=31.12.2021, startHour=11, startMinutes=0, endHour=17, endMinutes=0, closed=false], " +
				"OpeningTime [attraction=1, date=01.01.2022, startHour=11, startMinutes=0, endHour=17, endMinutes=0, closed=false], " +
				"OpeningTime [attraction=2, date=31.12.2021, closed=true], " +
				"OpeningTime [attraction=2, date=01.01.2022, startHour=9, startMinutes=0, endHour=20, endMinutes=0, closed=false]]",
				openingTimes.getOpeningTimes(new DateValue(31, 12, 2021), new DateValue(1, 1, 2022)).toString());

		assertEquals(24, openingTimes.getOpeningTimes(new DateValue(25, 12, 2021), new DateValue(5, 1, 2022)).size());

		assertEquals(4384, openingTimes.getOpeningTimes(new DateValue(1, 1, 2000), new DateValue(31, 12, 2005)).size());
	}

}
