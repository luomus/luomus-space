package fi.luomus.spacereservation.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fi.luomus.spacereservation.model.Attraction;
import fi.luomus.spacereservation.util.OpeningTimesParseUtil;
import fi.luomus.spacereservation.util.OpeningTimesParseUtil.Result;

public class OpeningTimesParseTests {

	private Attraction attraction = new Attraction(1, null);

	@Test
	public void test_empty_data() {
		Result result = new OpeningTimesParseUtil(attraction, 2022).parse("");
		assertTrue(result.getOpeningTimes().isEmpty());
		assertEquals("[1.1.: tiedot puuttuvat, 2.1.: tiedot puuttuvat]", result.getErrors());
	}

	@Test
	public void test_basic_parsing() {
		Result result = new OpeningTimesParseUtil(attraction, 2022).parse("9-18\nsuljettu\n---------\n  9.15 - 16  \n9.0-16.45\n9.00-16\n9-16.45");
		assertEquals("[" +
				"OpeningTime [attraction=1, date=01.01.2022, startHour=9, startMinutes=0, endHour=18, endMinutes=0, closed=false], " +
				"OpeningTime [attraction=1, date=02.01.2022, closed=true], " +
				"OpeningTime [attraction=1, date=03.01.2022, startHour=9, startMinutes=15, endHour=16, endMinutes=0, closed=false], " +
				"OpeningTime [attraction=1, date=04.01.2022, startHour=9, startMinutes=0, endHour=16, endMinutes=45, closed=false], " +
				"OpeningTime [attraction=1, date=05.01.2022, startHour=9, startMinutes=0, endHour=16, endMinutes=0, closed=false], " +
				"OpeningTime [attraction=1, date=06.01.2022, startHour=9, startMinutes=0, endHour=16, endMinutes=45, closed=false]]",
				result.getOpeningTimes().toString());
		assertEquals("[7.1.: tiedot puuttuvat]", result.getErrors());
	}

	@Test
	public void test_invalid_time() {
		Result result = new OpeningTimesParseUtil(attraction, 2022).parse("9\n");
		assertTrue(result.getOpeningTimes().isEmpty());
		assertEquals("[1.1.: epäkelpo kellonaika, pitäisi olla alku-loppu, 2.1.: tiedot puuttuvat]", result.getErrors());
	}

	@Test
	public void test_invalid_hour() {
		Result result = new OpeningTimesParseUtil(attraction, 2022).parse("9-\n");
		assertTrue(result.getOpeningTimes().isEmpty());
		assertEquals("[1.1.: epäkelpo kellonaika, pitäisi olla alku-loppu, 2.1.: tiedot puuttuvat]", result.getErrors());
	}

	@Test
	public void all_ok() {
		String data = "";
		for (int i = 1; i<=365; i++) {
			data += "9-18\n";
		}
		Result result = new OpeningTimesParseUtil(attraction, 2022).parse(data);
		assertEquals(365, result.getOpeningTimes().size());
		assertEquals("", result.getErrors());
	}

	@Test
	public void too_many_days() {
		String data = "";
		for (int i = 1; i<=367; i++) {
			data += "9-18\n";
		}
		Result result = new OpeningTimesParseUtil(attraction, 2022).parse(data);
		assertEquals(365, result.getOpeningTimes().size());
		assertEquals("[Liian paljon päiviä!]", result.getErrors());
	}

}
