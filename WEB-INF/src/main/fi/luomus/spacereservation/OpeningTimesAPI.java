package fi.luomus.spacereservation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.spacereservation.dao.DAO;
import fi.luomus.spacereservation.model.Attraction;
import fi.luomus.spacereservation.model.OpeningTime;
import fi.luomus.spacereservation.model.OpeningTimes;

@WebServlet(urlPatterns = {"/opening-times"})
public class OpeningTimesAPI extends CalendarServlet {

	private static final long serialVersionUID = 7856699166661121047L;

	@Override
	protected boolean authorized(HttpServletRequest req) {
		return true;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			DateValue day = getDate(req, "day");
			DateValue startDay = getDate(req, "start_day");
			DateValue endDay = getDate(req, "end_day");
			List<Integer> attractionIds = parseAttractionIds(req.getParameter("location"));
			List<OpeningTime> openingTimes = getOpeningTimes(day, startDay, endDay);
			if (!attractionIds.isEmpty()) {
				openingTimes = getFiltered(openingTimes, attractionIds);
			}
			return jsonResponse(toJson(openingTimes));
		} catch (IllegalArgumentException e) {
			return error(400, res, e.getMessage());
		} catch (Exception e) {
			return error(500, res, LogUtils.buildStackTrace(e, 10));
		}
	}

	private List<OpeningTime> getOpeningTimes(DateValue day, DateValue startDay, DateValue endDay) throws Exception {
		DAO dao = getDao();
		List<Attraction> attractions = dao.getAttractions();
		if (day != null) {
			return new OpeningTimes(dao.getOpeningTimes(day.getYearAsInt()), attractions).getOpeningTimes(day);
		}
		if (startDay != null && endDay != null) {
			if (startDay.getYearAsInt() == endDay.getYearAsInt()) {
				return new OpeningTimes(dao.getOpeningTimes(startDay.getYearAsInt()), attractions).getOpeningTimes(startDay, endDay);
			}
			List<OpeningTime> combined = new ArrayList<>();
			combined.addAll(dao.getOpeningTimes(startDay.getYearAsInt()));
			combined.addAll(dao.getOpeningTimes(endDay.getYearAsInt()));
			return new OpeningTimes(combined, attractions).getOpeningTimes(startDay, endDay);
		}
		throw new IllegalArgumentException("Must give day OR start_day, end_day");
	}

	private DateValue getDate(HttpServletRequest req, String param) {
		String v = req.getParameter(param);
		if (v == null) return null;
		try {
			return DateUtils.convertToDateValue(v, "yyyy-MM-dd");
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid " + param);
		}
	}

	private List<Integer> parseAttractionIds(String v) {
		if (v == null) return Collections.emptyList();
		List<Integer> ids = new ArrayList<>();
		try {
			for (String s : v.split(Pattern.quote(","))) {
				s = s.trim();
				ids.add(Integer.valueOf(s));
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("Invalid location_id");
		}
		return ids;
	}

	private List<OpeningTime> getFiltered(List<OpeningTime> openingTimes, List<Integer> attractionIds) {
		return openingTimes.stream().filter(o -> attractionIds.contains(o.getAttraction().getId())).collect(Collectors.toList());
	}

	private JSONObject toJson(List<OpeningTime> openingTimes) {
		JSONObject json = new JSONObject();
		JSONArray results = json.getArray("results");
		for (OpeningTime o : openingTimes) {
			results.appendObject(toJson(o));
		}
		return json;
	}

	// results:
	//   array
	//		"office": 9,
	//    	"type": "special_day",
	//    	"day": "2021-06-23",
	//    	"open_time": "closed",
	//    	"close_time": "closed"
	//
	//		"office": 9,
	//    	"type": "default",
	//    	"day": "2021-06-23",
	//    	"open_time": "10.00",
	//    	"close_time": "19.00"

	private JSONObject toJson(OpeningTime o) {
		JSONObject json = new JSONObject();
		json.setInteger("location_id", o.getAttraction().getId());
		json.setObject("location_name", toJson(o.getAttraction().getName()));
		json.setString("day", toIsoDate(o));
		if (o.isClosed()) {
			json.setString("open_time", "closed");
			json.setString("close_time", "closed");
		} else {
			json.setString("open_time", toTime(o.getStartHour(), o.getStartMinutes()));
			json.setString("close_time", toTime(o.getEndHour(), o.getEndMinutes()));
		}
		return json;
	}

	private JSONObject toJson(LocalizedText name) {
		JSONObject json = new JSONObject();
		json.setString("fi", name.forLocale("fi"));
		json.setString("en", name.forLocale("en"));
		json.setString("sv", name.forLocale("sv"));
		json.setString("ru", name.forLocale("ru"));
		return json;
	}

	private String toIsoDate(OpeningTime o) {
		return DateUtils.format(o.getDate(), "yyyy-MM-dd");
	}

	private String toTime(int hour, int minutes) {
		return hour + "." + frontZero(minutes);
	}

	private String frontZero(int minutes) {
		String s = ""+minutes;
		if (s.length() == 2) return s;
		return "0" + s;
	}

	private ResponseData error(int status, HttpServletResponse res, String message) {
		res.setStatus(status);
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.setInteger("status", status);
		jsonResponse.setString("message", message);
		return jsonResponse(jsonResponse);
	}
}
