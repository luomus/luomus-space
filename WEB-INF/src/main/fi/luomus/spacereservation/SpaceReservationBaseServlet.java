package fi.luomus.spacereservation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.services.BaseServlet;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.Utils;
import fi.luomus.spacereservation.dao.DAO;
import fi.luomus.spacereservation.dao.DAOImple;
import fi.luomus.spacereservation.dao.OracleDataSourceDefinition;
import fi.luomus.spacereservation.util.LoginUtil;

public abstract class SpaceReservationBaseServlet extends BaseServlet {

	private static final long serialVersionUID = 5270251244465572696L;

	private static DAOImple dao;
	private static HikariDataSource oracleDataSource;

	@Override
	protected void applicationInitOnlyOnce() {
		oracleDataSource = OracleDataSourceDefinition.initDataSource(getConfig().connectionDescription());
		dao = new DAOImple(oracleDataSource);
	}

	@Override
	protected void applicationDestroy() {
		closeDataSource();
	}

	private void closeDataSource() {
		if (oracleDataSource != null) {
			try {
				oracleDataSource.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected DAO getDao() {
		return dao;
	}

	@Override
	protected void applicationInit() {}

	@Override
	protected String configFileName() {
		return "luomus-space.properties";
	}

	@Override
	protected boolean authorized(HttpServletRequest req) {
		SessionHandler session = getSession(req);
		return session.isAuthenticatedFor(getConfig().systemId());
	}

	@Override
	protected ResponseData notAuthorizedRequest(HttpServletRequest req, HttpServletResponse res) {
		return getLoginUtil().processGet(next());
	}

	protected String next() {
		return "";
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	private static LoginUtil util = null;

	protected LoginUtil getLoginUtil() {
		if (util == null) {
			util = new LoginUtil(Utils.set("MA.admin", "MA.luomusSpaceCalendarUser", "MA.luomusSpaceOpeningTimesUser"), getConfig(), getErrorReporter());
		}
		return util;
	}

}
