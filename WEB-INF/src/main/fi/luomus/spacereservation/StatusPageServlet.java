package fi.luomus.spacereservation;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/status"})
public class StatusPageServlet extends CalendarServlet {

	private static final long serialVersionUID = 8690385556150739629L;

	@Override
	protected boolean authorized(HttpServletRequest req) {
		return true;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		if (spaces().isEmpty()) throw new IllegalStateException("Spaces fail to load");
		if (reservations(2022, "2022-01-03").getAll().isEmpty()) throw new IllegalStateException("Reservations fail to load");
		return new ResponseData("ok", "text/plain");
	}

}
