package fi.luomus.spacereservation.dao;

import java.util.List;
import java.util.Map;

import fi.luomus.spacereservation.model.Attraction;
import fi.luomus.spacereservation.model.OpeningTime;
import fi.luomus.spacereservation.model.Reservation;
import fi.luomus.spacereservation.model.Space;

public interface DAO {

	List<Reservation> getReservations(int year, String weekId) throws Exception;

	void persist(Reservation reservation) throws Exception;

	void delete(int id) throws Exception;

	Map<String, Space> getSpaces() throws Exception;

	List<OpeningTime> getOpeningTimes(int year) throws Exception;

	void persist(List<OpeningTime> openingTimes) throws Exception;

	List<Attraction> getAttractions() throws Exception;

}
