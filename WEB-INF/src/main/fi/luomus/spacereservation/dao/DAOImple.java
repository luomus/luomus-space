package fi.luomus.spacereservation.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;
import fi.luomus.spacereservation.model.Attraction;
import fi.luomus.spacereservation.model.OpeningTime;
import fi.luomus.spacereservation.model.Reservation;
import fi.luomus.spacereservation.model.Space;
import fi.luomus.spacereservation.util.WeekUtil;

public class DAOImple implements DAO {

	private final HikariDataSource oracleDataSource;

	public DAOImple(HikariDataSource oracleDataSource) {
		this.oracleDataSource = oracleDataSource;
	}

	private TransactionConnection getOracleConnection() throws SQLException {
		return new SimpleTransactionConnection(oracleDataSource.getConnection());
	}

	@Override
	public List<Reservation> getReservations(int year, String weekId) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			List<Reservation> reservations = new ArrayList<>();
			con = getOracleConnection();
			p = con.prepareStatement("" +
					" SELECT id, space_id, day, month, year, start_hour, start_minutes, end_hour, end_minutes, person_name, event_name, event_desc " +
					" FROM reservation " +
					" WHERE year = ? AND week = ? AND deleted IS NULL " +
					" ORDER BY id ");
			p.setInt(1, year);
			p.setString(2, weekId);
			rs = p.executeQuery();
			while (rs.next()) {
				reservations.add(buildReservation(rs));
			}
			return reservations;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	private Reservation buildReservation(ResultSet rs) throws SQLException {
		return new Reservation(rs.getInt(1), rs.getInt(2), new DateValue(rs.getInt(3), rs.getInt(4), rs.getInt(5)), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10), rs.getString(11), rs.getString(12));
	}

	@Override
	public void persist(Reservation reservation) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		try {
			con = getOracleConnection();
			p = con.prepareStatement("" +
					" INSERT INTO reservation (space_id, day, month, year, week, start_hour, start_minutes, end_hour, end_minutes, person_name, event_name, event_desc) " +
					" VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ");
			p.setInt(1, reservation.getSpaceId());
			p.setInt(2, reservation.getDate().getDayAsInt());
			p.setInt(3, reservation.getDate().getMonthAsInt());
			p.setInt(4, reservation.getDate().getYearAsInt());
			p.setString(5, WeekUtil.getWeekId(reservation.getDate()));
			p.setInt(6, reservation.getStartHour());
			p.setInt(7, reservation.getStartMinutes());
			p.setInt(8, reservation.getEndHour());
			p.setInt(9, reservation.getEndMinutes());
			p.setString(10, reservation.getPersonName());
			p.setString(11, reservation.getEventName());
			p.setString(12, reservation.getEventDesc());
			p.execute();
		} finally {
			Utils.close(p,con);
		}
	}

	@Override
	public void delete(int id) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		try {
			con = getOracleConnection();
			p = con.prepareStatement(" UPDATE reservation SET deleted = 1 WHERE id = ? ");
			p.setInt(1, id);
			p.execute();
		} finally {
			Utils.close(p, con);
		}
	}

	@Override
	public Map<String, Space> getSpaces() throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			Map<String, Space> spaces = new LinkedHashMap<>();
			con = getOracleConnection();
			p = con.prepareStatement(" SELECT id, name, name_short FROM space ORDER BY name");
			rs = p.executeQuery();
			while (rs.next()) {
				Space space = new Space(rs.getInt(1), rs.getString(2), rs.getString(3));
				spaces.put(String.valueOf(space.getId()), space);
			}
			return spaces;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public List<OpeningTime> getOpeningTimes(int year) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			List<OpeningTime> openingTimes = new ArrayList<>();
			con = getOracleConnection();
			p = con.prepareStatement("" +
					" SELECT id, attraction_id, day, month, year, start_hour, start_minutes, end_hour, end_minutes " +
					" FROM openingtime " +
					" WHERE year = ? " +
					" ORDER BY year, month, day, attraction_id ");
			p.setInt(1, year);
			rs = p.executeQuery();
			while (rs.next()) {
				openingTimes.add(buildOpeningTime(rs));
			}
			return openingTimes;
		} finally {
			Utils.close(p, rs, con);
		}
	}

	private OpeningTime buildOpeningTime(ResultSet rs) throws Exception {
		return new OpeningTime(rs.getInt(1), getAttraction(rs.getInt(2)), new DateValue(rs.getInt(3), rs.getInt(4), rs.getInt(5)), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));
	}

	private Attraction getAttraction(int id) throws Exception {
		for (Attraction a : getAttractions()) {
			if (id == a.getId()) return a;
		}
		throw new IllegalArgumentException("Unknown attraction " + id);
	}

	@Override
	public void persist(List<OpeningTime> openingTimes) throws Exception {
		try (TransactionConnection con = getOracleConnection();
				PreparedStatement existsStmt = otExists(con);
				PreparedStatement deleteStmt = otDelete(con);
				PreparedStatement insertStmt = otInsert(con);
				PreparedStatement updateStmt = otUpdate(con)) {
			con.startTransaction();
			for (OpeningTime o : openingTimes) {
				persists(o, existsStmt, deleteStmt, insertStmt, updateStmt);
			}
			con.commitTransaction();
		}
	}

	private void persists(OpeningTime o, PreparedStatement existsStmt, PreparedStatement deleteStmt, PreparedStatement insertStmt, PreparedStatement updateStmt) throws SQLException {
		Integer existingId = otExists(o, existsStmt);
		if (existingId != null) {
			if (o.isClosed()) {
				otDelete(existingId, deleteStmt);
			} else {
				otUpdate(existingId, o, updateStmt);
			}
		} else {
			if (!o.isClosed()) {
				otInsert(o, insertStmt);
			}
		}
	}

	private void otInsert(OpeningTime o, PreparedStatement p) throws SQLException {
		p.setInt(1, o.getAttraction().getId());
		p.setInt(2, o.getDate().getDayAsInt());
		p.setInt(3, o.getDate().getMonthAsInt());
		p.setInt(4, o.getDate().getYearAsInt());
		p.setInt(5, o.getStartHour());
		p.setInt(6, o.getStartMinutes());
		p.setInt(7, o.getEndHour());
		p.setInt(8, o.getEndMinutes());
		p.execute();
	}

	private void otUpdate(Integer openingTimesId, OpeningTime o, PreparedStatement p) throws SQLException {
		p.setInt(1, o.getStartHour());
		p.setInt(2, o.getStartMinutes());
		p.setInt(3, o.getEndHour());
		p.setInt(4, o.getEndMinutes());
		p.setInt(5, openingTimesId);
		int i = p.executeUpdate();
		if (i != 1) throw new IllegalStateException("Updated " + i + " rows instead of 1: " + o);
	}

	private void otDelete(Integer openingTimesId, PreparedStatement p) throws SQLException {
		p.setInt(1, openingTimesId);
		int i = p.executeUpdate();
		if (i != 1) throw new IllegalStateException("Deleted " + i + " rows instead of 1: " + openingTimesId);
	}

	private Integer otExists(OpeningTime o, PreparedStatement p) throws SQLException {
		p.setInt(1, o.getDate().getYearAsInt());
		p.setInt(2, o.getDate().getMonthAsInt());
		p.setInt(3, o.getDate().getDayAsInt());
		p.setInt(4, o.getAttraction().getId());
		try (ResultSet rs = p.executeQuery()) {
			if (rs.next()) {
				return rs.getInt(1);
			}
			return null;
		}
	}

	private PreparedStatement otUpdate(TransactionConnection con) throws SQLException {
		return con.prepareStatement("UPDATE openingtime SET start_hour = ?, start_minutes = ?, end_hour = ?, end_minutes = ? WHERE id = ? ");
	}

	private PreparedStatement otInsert(TransactionConnection con) throws SQLException {
		return con.prepareStatement("" +
				" INSERT INTO openingtime (attraction_id, day, month, year, start_hour, start_minutes, end_hour, end_minutes) " +
				" VALUES (?,?,?,?,?,?,?,?) ");
	}

	private PreparedStatement otDelete(TransactionConnection con) throws SQLException {
		return con.prepareStatement("DELETE FROM openingtime WHERE id = ? ");
	}

	private PreparedStatement otExists(TransactionConnection con) throws SQLException {
		return con.prepareStatement("" +
				" SELECT id " +
				" FROM openingtime " +
				" WHERE year = ? AND month = ? AND day = ? AND attraction_id = ? ");
	}

	@Override
	public List<Attraction> getAttractions() throws Exception {
		return Attraction.ATTRACTIONS;
	}

}
