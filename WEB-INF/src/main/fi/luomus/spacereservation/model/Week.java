package fi.luomus.spacereservation.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Week {

	private final String weekId;
	private final int weekNumber;
	private final LocalDate monday;
	private final LocalDate sunday;

	public Week(String weekId, int weekNumber, LocalDate monday, LocalDate sunday) {
		this.weekId = weekId;
		this.weekNumber = weekNumber;
		this.monday = monday;
		this.sunday = sunday;
	}

	public String getWeekId() {
		return weekId;
	}

	@Override
	public String toString() {
		return dateString(monday) + " - " + dateString(sunday) + " (" + weekNumber + ")";
	}

	private String dateString(LocalDate date) {
		return date.getDayOfMonth() + "." + date.getMonthValue() + ".";
	}

	public List<LocalDate> getDays() {
		List<LocalDate> days = new ArrayList<>();
		LocalDate day = LocalDate.from(monday);
		for (int i = 1; i<= 7; i++) {
			days.add(day);
			day = day.plusDays(1);
		}
		return days;
	}

	public String getString() {
		return toString();
	}

}
