package fi.luomus.spacereservation.model;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.DateUtils;

public class OpeningTimes {

	private final List<OpeningTime> openingTimes;
	private final List<Attraction> attractions;

	public OpeningTimes(List<OpeningTime> openingTimes, List<Attraction> attractions) {
		this.openingTimes = Collections.unmodifiableList(openingTimes);
		this.attractions = attractions;
	}

	public OpeningTime getOpeningTime(LocalDate date, int attractionId) {
		for (OpeningTime t : getOpeningTimes(toDateValue(date))) {
			if (t.getAttraction().getId() == attractionId) return t;
		}
		return null;
	}

	public List<OpeningTime> getOpeningTimes(DateValue date) {
		return getOpeningTimes(date, date);
	}

	public List<OpeningTime> getOpeningTimes(DateValue begin, DateValue end) {
		int min = toInt(begin);
		int max = toInt(end);
		if (min > max) throw new IllegalArgumentException("end is before start");
		List<OpeningTime> times = new ArrayList<>();
		Set<String> searchMap = new HashSet<>();
		for (OpeningTime o : openingTimes) {
			int thisD = toInt(o.getDate());
			if (thisD >= min && thisD <= max) {
				times.add(o);
				searchMap.add(o.getAttraction().getId()+"|"+thisD);
			}
		}
		for (Attraction a : attractions) {
			for (int date = min; date<=max; date++) {
				if (!searchMap.contains(a.getId()+"|"+date)) {
					DateValue dv = toDate(date);
					if (dv == null) continue; // not a calendar date
					times.add(OpeningTime.closed(a, dv));
				}
			}
		}
		Collections.sort(times);
		return times;
	}

	private DateValue toDate(int date) {
		try {
			return DateUtils.convertToDateValue(DateUtils.convertToDate(""+date, "yyyyMMdd"));
		} catch (ParseException e) {
			return null;
		}
	}

	private int toInt(DateValue d) {
		return Integer.valueOf(DateUtils.format(DateUtils.convertToDate(d), "yyyyMMdd"));
	}


	private DateValue toDateValue(LocalDate date) {
		return new DateValue(date.getDayOfMonth(), date.getMonthValue(), date.getYear());
	}

}