package fi.luomus.spacereservation.model;

import fi.luomus.commons.containers.DateValue;

public class OpeningTime implements Comparable<OpeningTime> {

	private final Integer id;
	private final Attraction attraction;
	private final DateValue date;
	private final int startHour;
	private final int startMinutes;
	private final int endHour;
	private final int endMinutes;
	private final boolean closed;

	public OpeningTime(Integer id, Attraction attraction, DateValue date, int startHour, int startMinutes, int endHour, int endMinutes) {
		this.id = id;
		this.attraction = attraction;
		this.date = date;
		this.startHour = startHour;
		this.startMinutes = startMinutes;
		this.endHour = endHour;
		this.endMinutes = endMinutes;
		this.closed = false;
	}

	public static OpeningTime closed(Attraction attraction, DateValue date) {
		return new OpeningTime(attraction, date);
	}

	private OpeningTime(Attraction attraction, DateValue date) {
		this.id = null;
		this.attraction = attraction;
		this.date = date;
		this.startHour = -1;
		this.startMinutes = -1;
		this.endHour = -1;
		this.endMinutes = -1;
		this.closed = true;
	}

	public Integer getId() {
		return id;
	}

	public Attraction getAttraction() {
		return attraction;
	}

	public DateValue getDate() {
		return date;
	}

	public int getStartHour() {
		return startHour;
	}

	public int getStartMinutes() {
		return startMinutes;
	}

	public int getEndHour() {
		return endHour;
	}

	public int getEndMinutes() {
		return endMinutes;
	}

	public boolean isClosed() {
		return closed;
	}

	@Override
	public String toString() {
		if (isClosed()) {
			return "OpeningTime [attraction=" + attraction.getId() + ", date=" + date + ", closed=" + closed + "]";
		}
		return "OpeningTime [attraction=" + attraction.getId() + ", date=" + date + ", startHour=" + startHour + ", startMinutes=" + startMinutes + ", endHour=" + endHour
				+ ", endMinutes=" + endMinutes + ", closed=" + closed + "]";
	}

	@Override
	public int compareTo(OpeningTime o) {
		int c = Integer.compare(this.getAttraction().getId(), o.getAttraction().getId());
		if (c != 0) return c;
		c = Integer.compare(this.getDate().getYearAsInt(), o.getDate().getYearAsInt());
		if (c != 0) return c;
		c = Integer.compare(this.getDate().getMonthAsInt(), o.getDate().getMonthAsInt());
		if (c != 0) return c;
		return Integer.compare(this.getDate().getDayAsInt(), o.getDate().getDayAsInt());
	}

}
