package fi.luomus.spacereservation.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.utils.Utils;

public class Reservations {

	private final Map<String, List<Reservation>> reservationsByDayHour = new HashMap<>();
	private final List<Reservation> reservations;

	public Reservations(List<Reservation> reservations) {
		this.reservations = Collections.unmodifiableList(reservations);
		for (Reservation r : reservations) {
			for (int hour : r.getHours()) {
				String key = toMapKey(r.getDate().getDayAsInt(), r.getDate().getMonthAsInt(), hour);
				if (!this.reservationsByDayHour.containsKey(key)) {
					this.reservationsByDayHour.put(key, new ArrayList<>());
				}
				this.reservationsByDayHour.get(key).add(r);				
			}
		}
		for (List<Reservation> l : reservationsByDayHour.values()) {
			Collections.sort(l, new Comparator<Reservation>() {
				@Override
				public int compare(Reservation r1, Reservation r2) {
					int c = r2.getLength().compareTo(r1.getLength());
					if (c != 0) return c;
					return Integer.compare(r1.getId(), r2.getId());
				}
			});
		}
	}

	private String toMapKey(int day, int month, int startHour) {
		return Utils.catenate("|", day, month, startHour);
	}

	public List<Reservation> getReservations(int day, int month, int startHour) {
		String key = toMapKey(day, month, startHour);
		if (reservationsByDayHour.containsKey(key)) {
			return Collections.unmodifiableList(reservationsByDayHour.get(key));
		}
		return Collections.emptyList();
	}

	public List<Reservation> getAll() {
		return reservations;
	}

}
