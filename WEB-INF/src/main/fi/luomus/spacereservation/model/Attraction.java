package fi.luomus.spacereservation.model;

import java.util.ArrayList;
import java.util.List;

import fi.luomus.commons.containers.LocalizedText;

public class Attraction {

	private final int id;
	private final LocalizedText name;

	public Attraction(int id, LocalizedText name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public LocalizedText getName() {
		return name;
	}

	public static final List<Attraction> ATTRACTIONS;
	static {
		ATTRACTIONS = new ArrayList<>();

		ATTRACTIONS.add(new Attraction(1, new LocalizedText()
				.set("fi", "Luonnontieteellinen museo")
				.set("en", "Natural History Museum")
				.set("sv", "Naturhistoriska museet")
				.set("ru", "Музей естествознания")));

		ATTRACTIONS.add(new Attraction(2, new LocalizedText()
				.set("fi", "Kaisaniemen kasvitieteellinen puutarha, kasvihuoneet")
				.set("en", "Kaisaniemi Botanic Garden, greenhouses")
				.set("sv", "Kajsaniemi botaniska trädgård, växthusen")
				.set("ru", "Ботанический сад Kaisaniemi")));

		ATTRACTIONS.add(new Attraction(3, new LocalizedText()
				.set("fi", "Kaisaniemen kasvitieteellinen puutarha, ulkopuutarha")
				.set("en", "Kaisaniemi Botanic Garden, outdoor garden")
				.set("sv", "Kajsaniemi botaniska trädgård, utomhusträdgården")
				.set("ru", "Открытый сад Kaisaniemi")));

		ATTRACTIONS.add(new Attraction(4, new LocalizedText()
				.set("fi", "Kumpulan kasvitieteellinen puutarha")
				.set("en", "Kumpula Botanic Garden")
				.set("sv", "Gumtäkt botaniska trädgård")
				.set("ru", "Ботанический сад Kumpula")));

		ATTRACTIONS.add(new Attraction(5, new LocalizedText()
				.set("fi", "Kumpulan geologiset kokoelmat")
				.set("en", "Kumpula Geological Collections")
				.set("sv", "Gumtäkt geologiska samlingar")
				.set("ru", "Геологические коллекции Kumpula")));
	}
}
