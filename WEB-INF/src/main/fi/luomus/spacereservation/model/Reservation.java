package fi.luomus.spacereservation.model;

import java.util.ArrayList;
import java.util.List;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.Utils;

public class Reservation {

	private final int id;
	private final int spaceId;
	private final DateValue date;
	private final int startHour;
	private final int startMinutes;
	private final int endHour;
	private final int endMinutes;
	private final String personName;
	private final String eventName;
	private final String eventDesc;
	private final String color;

	private static final List<String> COLORS = Utils.list("rgb(255,200,200)", "rgb(200,255,200)", "rgb(200,200,255)", "rgb(255,255,200)", "rgb(200,255,255)", "rgb(255,200,255)");
	private static int colorI = 0;
	
	public Reservation(int id, int spaceId, DateValue date, int startHour, int startMinutes, int endHour, int endMinutes, String personName, String eventName, String eventDesc) {
		this.id = id;
		this.spaceId = spaceId;
		this.date = date;
		this.startHour = startHour;
		this.startMinutes = startMinutes;
		this.endHour = endHour;
		this.endMinutes = endMinutes;
		this.personName = s(personName);
		this.eventName = s(eventName);
		this.eventDesc = s(eventDesc);
		this.color = nextColor();
	}

	public Reservation(Reservation r, DateValue date) {
		this(-1, r.getSpaceId(), date, r.getStartHour(), r.getStartMinutes(), r.getEndHour(), r.getEndMinutes(), r.getPersonName(), r.getEventName(), r.getEventDesc());
	}
	
	private String nextColor() {
		if (colorI >= COLORS.size()) colorI = 0;
		try {
			return COLORS.get(colorI++);
		} catch (IndexOutOfBoundsException e) {
			colorI = 1;
			return COLORS.get(0);
		}
	}

	private String s(String s) {
		if (s == null) return "";
		s = s.replace("\n", " ").replace("\r", " ").trim();
		while (s.contains("  ")) {
			s = s.replace("  ", " ");
		}
		return s.trim();
	}

	public int getId() {
		return id;
	}

	public int getSpaceId() {
		return spaceId;
	}

	public DateValue getDate() {
		return date;
	}

	public int getStartHour() {
		return startHour;
	}

	public int getStartMinutes() {
		return startMinutes;
	}

	public int getEndHour() {
		return endHour;
	}

	public int getEndMinutes() {
		return endMinutes;
	}

	public String getPersonName() {
		return personName;
	}

	public String getEventName() {
		return eventName;
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public String getTime() {
		return startHour + ":" + zero(startMinutes) + " - " + endHour + ":" + zero(endMinutes);
	}

	private String zero(int minutes) {
		String s = String.valueOf(minutes);
		if (s.length() == 1) return "0"+s;
		return s;
	}

	public List<Integer> getHours() {
		List<Integer> hours = new ArrayList<>();
		for (int hour = startHour; hour <= endHour; hour++) {
			if (hour == endHour && endMinutes == 0) break;
			hours.add(hour);
		}
		return hours;
	}

	public String getColor() {
		return color;
	}

	public Integer getLength() {
		return (endHour * 100 + endMinutes) - (startHour * 100 + startMinutes);
	}

}
