package fi.luomus.spacereservation.model;

public class Space {

	private final int id;
	private final String name;
	private final String nameShort;

	public Space(int id, String name, String nameShort) {
		this.id = id;
		this.name = name;
		this.nameShort = nameShort;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getNameShort() {
		return nameShort;
	}

}
