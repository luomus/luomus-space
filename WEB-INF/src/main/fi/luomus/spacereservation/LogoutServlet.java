package fi.luomus.spacereservation;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.session.SessionHandler;

@WebServlet(urlPatterns = {"/logout/*"})
public class LogoutServlet extends SpaceReservationBaseServlet {

	private static final long serialVersionUID = 7604078775327633292L;

	@Override
	protected boolean authorized(HttpServletRequest req) {
		return true;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		SessionHandler sessionHandler = getSession(req);
		getLoginUtil().logOut(sessionHandler);
		if (sessionHandler.hasSession()) {
			sessionHandler.invalidate();
		}
		return redirectTo("http://luomus.fi");
	}

}
