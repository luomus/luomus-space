package fi.luomus.spacereservation;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.spacereservation.model.Attraction;
import fi.luomus.spacereservation.model.OpeningTime;
import fi.luomus.spacereservation.model.OpeningTimes;
import fi.luomus.spacereservation.util.OpeningTimesParseUtil;
import fi.luomus.spacereservation.util.OpeningTimesParseUtil.Result;
import fi.luomus.spacereservation.util.WeekUtil;

@WebServlet(urlPatterns = {"/attractions/*"})
public class AttractionsServlet extends SpaceReservationBaseServlet {

	private static final long serialVersionUID = 5346500820117688670L;

	private boolean hasRole(HttpServletRequest req) {
		SessionHandler session = getSession(req);
		if (!session.isAuthenticatedFor(getConfig().systemId())) return false;
		return "MA.luomusSpaceOpeningTimesUser".equals(session.userType());
	}

	@Override
	protected String next() {
		return "/attractions/";
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		if (!hasRole(req)) {
			return noRole(req);
		}
		int year = getYear(req);
		List<LocalDate> days = WeekUtil.getDays(year);
		List<Attraction> attractions = getDao().getAttractions();

		SessionHandler ses = getSession(req);
		String prevData = (String) ses.getObject("prevData");
		ses.setObject("prevData", null);

		return new ResponseData()
				.setViewName("attractions")
				.setData("username", getSession(req).userName())
				.setData("year", year)
				.setData("openingTimes", new OpeningTimes(getDao().getOpeningTimes(year), attractions))
				.setData("attractions", attractions)
				.setData("days", days)
				.setData("daysCount", daysCount(days))
				.setData("today", LocalDate.now().toEpochDay())
				.setData("saveErrors", ses.getFlashError())
				.setData("saveSuccess", ses.getFlashSuccess())
				.setData("prevData", prevData)
				.setData("prevAttraction", ses.getObject("prevAttraction"));
	}

	private ResponseData noRole(HttpServletRequest req) {
		getSession(req).setFlashError("Tarvittava rooli aukioloaikojen muokkaamiseksi puuttuu! Ota yheytta it-support@luomus.fi");
		return redirectTo(getConfig().baseURL());
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		if (!hasRole(req)) {
			return noRole(req);
		}
		int year = getYear(req);
		int attractionId = Integer.valueOf(req.getParameter("attraction"));
		String data = req.getParameter("data");
		Result result = new OpeningTimesParseUtil(getAttraction(attractionId), year).parse(data);

		SessionHandler ses = getSession(req);
		if (!result.getErrors().isEmpty()) {
			ses.setFlashError(result.getErrors());
			ses.setObject("prevData", data);
			ses.setObject("prevAttraction", attractionId);
		} else {
			save(result.getOpeningTimes());
			ses.setFlashSuccess("Tallennettu! Tarkista vielä tulkitut tiedot!");
			ses.setObject("prevData", null);
		}
		return redirectTo(getConfig().baseURL()+"/attractions?year="+year);
	}

	private Attraction getAttraction(int attractionId) throws Exception {
		for (Attraction a : getDao().getAttractions()) {
			if (a.getId() == attractionId) return a;
		}
		throw new IllegalStateException("Uknown attraction " + attractionId);
	}

	private void save(List<OpeningTime> openingTimes) throws Exception {
		getDao().persist(openingTimes);
	}

	private int daysCount(List<LocalDate> days) {
		int i = 0;
		for (LocalDate d : days) {
			i++;
			if (d.getDayOfWeek() ==  DayOfWeek.MONDAY) i++;
		}
		return i;
	}

	private int getYear(HttpServletRequest req) {
		int currentYear = DateUtils.getCurrentYear();
		if (req.getParameter("year") != null) {
			try {
				int year = Integer.valueOf(req.getParameter("year"));
				if (year <= 2050) {
					return year;
				}
			} catch (Exception e) {}
		}
		return currentYear;
	}

}
