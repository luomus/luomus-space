package fi.luomus.spacereservation.util;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.Utils;
import fi.luomus.spacereservation.model.Attraction;
import fi.luomus.spacereservation.model.OpeningTime;

public class OpeningTimesParseUtil {

	public static class Result {
		private List<OpeningTime> openingTimes = new ArrayList<>();
		private List<String> errors = new ArrayList<>();
		public List<OpeningTime> getOpeningTimes() {
			return openingTimes;
		}
		public void setOpeningTimes(List<OpeningTime> openingTimes) {
			this.openingTimes = openingTimes;
		}
		public String getErrors() {
			if (errors.isEmpty()) return "";
			return errors.toString();
		}
		public void setErrors(List<String> errors) {
			this.errors = errors;
		}
	}

	private final Attraction attraction;
	private final Integer year;

	public OpeningTimesParseUtil(Attraction attraction, Integer year) {
		this.attraction = attraction;
		this.year = year;
	}

	public Result parse(String data) {
		Result result = new Result();
		Iterator<LocalDate> days = WeekUtil.getDays(year).iterator();
		for (String line : data.replace("\r", "").split(Pattern.quote("\n"))) {
			line = clean(line);
			if (line.startsWith("---")) continue; // week separator
			if (!days.hasNext()) {
				result.errors.add("Liian paljon päiviä!");
				return result;
			}
			parse(line, days.next(), result);
		}
		if (days.hasNext()) {
			result.errors.add(debug(days.next()) + ": tiedot puuttuvat");
		}
		return result;
	}

	private void parse(String line, LocalDate date, Result result) {
		try {
			result.openingTimes.add(tryParse(line, date));
		} catch (Exception e) {
			result.errors.add(e.getMessage());
		}
	}

	private OpeningTime tryParse(String line, LocalDate date) {
		if (line.isEmpty()) throw new IllegalArgumentException(debug(date) + ": tiedot puuttuvat");
		if (line.equals("suljettu")) return OpeningTime.closed(attraction, date(date));
		String[] parts = line.split(Pattern.quote("-"));
		if (parts.length != 2) throw new IllegalArgumentException(debug(date) + ": epäkelpo kellonaika, pitäisi olla alku-loppu");
		try {
			return new OpeningTime(-1, attraction, date(date), parseHour(parts[0]), parseMinutes(parts[0]), parseHour(parts[1]), parseMinutes(parts[1]));
		} catch (Exception e) {
			throw new IllegalArgumentException(debug(date) + ": " + e.getMessage());
		}
	}

	private int parseMinutes(String s) {
		if (!s.contains(".")) return 0;
		String[] parts = s.split(Pattern.quote("."));
		if (parts.length != 2) throw new IllegalArgumentException("HH.MM erotin on piste, liian paljon pisteitä");
		try {
			int i = Integer.valueOf(parts[1]);
			if (i < 0) throw new IllegalArgumentException("Minuutti pienempi kuin 0");
			if (i > 59) throw new IllegalArgumentException("Minuutti pienempi kuin 59");
			return i;
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Minuutti ei ole luku");
		}
	}

	private int parseHour(String s) {
		String hour = s;
		if (s.contains(".")) {
			String[] parts = s.split(Pattern.quote("."));
			if (parts.length != 2) throw new IllegalArgumentException("HH.MM erotin on piste, liian paljon pisteitä");
			hour = parts[0];
		}
		try {
			int i = Integer.valueOf(hour);
			if (i < 1) throw new IllegalArgumentException("Tunti on pienempi kuin 1");
			if (i > 23) throw new IllegalArgumentException("Tunti on suurempi kuin 23");
			return i;
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Tunti ei ole luku");
		}
	}

	private String debug(LocalDate date) {
		return date.getDayOfMonth()+"."+date.getMonthValue() + ".";
	}

	private DateValue date(LocalDate date) {
		return new DateValue(date.getDayOfMonth(), date.getMonthValue(), date.getYear());
	}

	private String clean(String line) {
		return Utils.removeWhitespace(line);
	}
}
