package fi.luomus.spacereservation.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.common.collect.Iterables;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.spacereservation.model.Week;

public class WeekUtil {

	private static final DateTimeFormatter WEEKID_PATTERN = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public static List<LocalDate> getWeekDays(int year, String weekId) {
		Week week = getWeeks(year).get(weekId);
		if (week == null) return Collections.emptyList();
		return week.getDays();
	}

	public static List<LocalDate> getDays(int year) {
		List<LocalDate> days = new ArrayList<>();
		LocalDate date = LocalDate.of(year, 1, 1);
		while (date.getYear() == year) {
			days.add(date);
			date = date.plusDays(1);
		}
		return days;
	}

	public static Map<String, Week> getWeeks(int year) {
		Map<String, Week> weeks = new LinkedHashMap<>();
		LocalDate date = LocalDate.of(year, 1, 1);
		LocalDate monday = getPrevMonday(date);
		int failsafe = 1;
		while (date.getYear() == year) {
			int week = monday.get(WeekFields.of(Locale.forLanguageTag("FI")).weekOfWeekBasedYear());
			String weekId = getWeekId(monday);
			LocalDate sunday = getNextSunday(date);
			weeks.put(weekId, new Week(weekId, week, monday, sunday));
			date = sunday.plusDays(1);
			monday = date;
			if (failsafe++ > 55) break;
		}
		return weeks;
	}

	public static String getWeekId(DateValue dateValue) {
		LocalDate date = LocalDate.of(dateValue.getYearAsInt(), dateValue.getMonthAsInt(), dateValue.getDayAsInt());
		LocalDate monday = getPrevMonday(date);
		return getWeekId(monday);
	}

	private static String getWeekId(LocalDate date) {
		return date.format(WEEKID_PATTERN);
	}

	private static LocalDate getPrevMonday(LocalDate date) {
		LocalDate monday = LocalDate.from(date);
		while (monday.getDayOfWeek() != DayOfWeek.MONDAY) {
			monday = monday.minusDays(1);
		}
		return monday;
	}

	private static LocalDate getNextSunday(LocalDate date) {
		LocalDate sunday = LocalDate.from(date);
		while (sunday.getDayOfWeek() != DayOfWeek.SUNDAY) {
			sunday = sunday.plusDays(1);
		}
		return sunday;
	}

	public static String getCurrentWeekId() {
		return getWeekId(getPrevMonday(LocalDate.now()));
	}

	public static DateValue addWeeks(DateValue date, int weeks) {
		LocalDate base = LocalDate.of(date.getYearAsInt(), date.getMonthAsInt(), date.getDayAsInt());
		LocalDate added = base.plusWeeks(weeks);
		return new DateValue(added.getDayOfMonth(), added.getMonthValue(), added.getYear());
	}

	public static String getNextWeekId(int year, String weekId) {
		try {
		LocalDate sunday = Iterables.getLast(getWeeks(year).get(weekId).getDays());
		LocalDate nextMonday = sunday.plusDays(1);
		if (nextMonday.getYear() != year) return null;
		return getWeekId(nextMonday);
		} catch (NullPointerException e) {
			return null;
		}
	}

}
