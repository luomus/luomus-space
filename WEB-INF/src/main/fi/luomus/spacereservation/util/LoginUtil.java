package fi.luomus.spacereservation.util;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajiauth.model.AuthenticationEvent;
import fi.luomus.lajiauth.model.UserDetails;
import fi.luomus.lajiauth.service.LajiAuthClient;
import fi.luomus.utils.exceptions.ApiException;

public class LoginUtil  {

	private static class AuthenticationResult {

		private final boolean success;
		private String token;
		private String errorMessage;
		private String userFullname;
		private Set<String> roles;
		private String next;

		public AuthenticationResult(boolean success) {
			this.success = success;
		}

		public boolean successful() {
			return success;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public String getUserFullname() {
			return userFullname;
		}

		public AuthenticationResult setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
			return this;
		}

		public void setUserFullname(String userFullname) {
			this.userFullname = userFullname;
		}

		public Set<String> getRoles() {
			return roles;
		}

		public void setRoles(Set<String> roles) {
			if (roles == null) roles = Collections.emptySet();
			this.roles = roles;
		}

		public String getNext() {
			return next;
		}

		public void setNext(String next) {
			this.next = next;
		}

		public String getToken() {
			return token;
		}

		public AuthenticationResult setToken(String token) {
			this.token = token;
			return this;
		}

	}

	private final ErrorReporter errorReporter;
	private final Config config;
	private final ObjectMapper objectMapper = new ObjectMapper();
	private final Set<String> acceptedRoles;

	public LoginUtil(Set<String> acceptedRoles, Config config, ErrorReporter errorReporter) {
		this.acceptedRoles = Collections.unmodifiableSet(acceptedRoles);
		this.config = config;
		this.errorReporter = errorReporter;
	}

	public ResponseData processGet(String next) {
		LajiAuthClient client = getLajiAuthClient();
		return new ResponseData().setRedirectLocation(client.createLoginUrl(next).build().toString());
	}

	public ResponseData processPost(HttpServletRequest req, SessionHandler session) throws Exception {
		String lajiAuthToken = req.getParameter("token");
		ResponseData responseData = new ResponseData().setViewName("login-error");
		AuthenticationResult authentication = authenticateViaLajiAuthentication(lajiAuthToken);
		try {
			if (authentication.successful()) {
				if (!hasRequiredRole(authentication)) {
					getLajiAuthClient().invalidateToken(lajiAuthToken);
					return responseData.setData("error", "Oikeudet puuttuvat. Ota yhteyttä it-support@luomus.fi ja pyydä pääsyä. \nPermission required, please contact it-support@luomus.fi ");
				}
				authenticateSession(session, authentication);
				String url = config.baseURL();
				if (authentication.getNext() != null) url += authentication.getNext();
				return responseData.setRedirectLocation(url);
			}
			return responseData.setData("error", authentication.getErrorMessage());
		} catch (Exception e) {
			errorReporter.report("Login data " + Utils.debugS(lajiAuthToken), e);
			return responseData.setData("error", "Something went wrong: " + e.getMessage());
		}
	}

	private boolean hasRequiredRole(AuthenticationResult authentication) {
		for (String role : authentication.getRoles()) {
			if (acceptedRoles.contains(role)) return true;
		}
		return false;
	}

	private void authenticateSession(SessionHandler session, AuthenticationResult authentication) throws Exception {
		session.authenticateFor(config.systemId());
		session.setUserId(authentication.getUserFullname());
		session.setUserName(authentication.getUserFullname());
		session.setTimeout(Integer.MAX_VALUE);
		session.setUserType(userType(authentication));
		session.setObject("token", authentication.getToken());
	}

	private String userType(AuthenticationResult authentication) {
		if (authentication.getRoles().contains("MA.admin")) return "MA.luomusSpaceOpeningTimesUser";
		if (authentication.getRoles().contains("MA.luomusSpaceOpeningTimesUser")) return "MA.luomusSpaceOpeningTimesUser";
		return "MA.luomusSpaceCalendarUser";
	}

	private AuthenticationResult authenticateViaLajiAuthentication(String token) throws Exception {
		AuthenticationEvent authorizationInfo = null;
		try {
			LajiAuthClient client = getLajiAuthClient();
			authorizationInfo = client.getAndValidateAuthenticationInfo(token);
			// Validation throws exception if something is wrong (ie for wrong system id); Authentication has been successful:
			return authenticationResultFromLajiAuth(authorizationInfo).setToken(token);
		} catch (Exception e) {
			if (authorizationInfo != null) {
				errorReporter.report("Erroreous LajiAuth login for " + Utils.debugS(token, objectMapper.writeValueAsString(authorizationInfo)), e);
			} else {
				errorReporter.report("Unsuccesful LajiAuth login for " + token, e);
			}
			return new AuthenticationResult(false).setErrorMessage(e.getMessage());
		}
	}

	private AuthenticationResult authenticationResultFromLajiAuth(AuthenticationEvent authenticationEvent) {
		AuthenticationResult authenticationResponse = new AuthenticationResult(true);
		UserDetails userDetails = authenticationEvent.getUser();
		authenticationResponse.setRoles(userDetails.getRoles());
		authenticationResponse.setUserFullname(userDetails.getName());
		authenticationResponse.setNext(authenticationEvent.getNext());
		return authenticationResponse;
	}

	private LajiAuthClient getLajiAuthClient() {
		try {
			return new LajiAuthClient(config.get("SystemQname"), new URI(config.get("LajiAuthURL")));
		} catch (Exception e) {
			throw new RuntimeException("Laji auth init", e);
		}
	}

	public void logOut(SessionHandler session) {
		String token = (String) session.getObject("token");
		if (token != null) {
			try {
				getLajiAuthClient().invalidateToken(token);
			} catch (ApiException e) {
			}
		}
	}

}
