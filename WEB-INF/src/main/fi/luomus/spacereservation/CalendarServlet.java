package fi.luomus.spacereservation;

import java.time.LocalDate;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.spacereservation.model.Reservations;
import fi.luomus.spacereservation.model.Space;
import fi.luomus.spacereservation.util.WeekUtil;

@WebServlet(urlPatterns = {"/*"})
public class CalendarServlet extends SpaceReservationBaseServlet {

	private static final long serialVersionUID = -7553946403611497036L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		int year = getYear(req);
		String weekId = getWeekId(req, year);
		return new ResponseData()
				.setViewName("calendar")
				.setData("username", getSession(req).userName())
				.setData("year", year)
				.setData("weekId", weekId)
				.setData("weeks", WeekUtil.getWeeks(year))
				.setData("weekDays", WeekUtil.getWeekDays(year, weekId))
				.setData("nextWeekId", WeekUtil.getNextWeekId(year, weekId))
				.setData("reservations", reservations(year, weekId))
				.setData("spaces", spaces())
				.setData("today", LocalDate.now().toEpochDay())
				.setData("roleError", getSession(req).getFlashError());
	}

	private static Map<String, Space> spaces = null;

	protected Map<String, Space> spaces() throws Exception {
		if (spaces == null) {
			spaces = getDao().getSpaces();
		}
		return spaces;
	}

	protected Reservations reservations(int year, String weekId) throws Exception {
		return new Reservations(getDao().getReservations(year, weekId));
	}

	private String getWeekId(HttpServletRequest req, int year) {
		if (req.getParameter("weekId") != null) {
			return req.getParameter("weekId");
		}
		if (year == DateUtils.getCurrentYear()) {
			return WeekUtil.getCurrentWeekId();
		}
		return WeekUtil.getWeeks(year).values().iterator().next().getWeekId();
	}

	private int getYear(HttpServletRequest req) {
		int currentYear = DateUtils.getCurrentYear();
		if (req.getParameter("year") != null) {
			try {
				int year = Integer.valueOf(req.getParameter("year"));
				if (year >= currentYear && year <= 2050) {
					return year;
				}
			} catch (Exception e) {}
		}
		return currentYear;
	}




}
