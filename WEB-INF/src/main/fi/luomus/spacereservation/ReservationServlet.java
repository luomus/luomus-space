package fi.luomus.spacereservation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.spacereservation.model.Reservation;
import fi.luomus.spacereservation.util.WeekUtil;

@WebServlet(urlPatterns = {"/reservation/*"})
public class ReservationServlet extends SpaceReservationBaseServlet {

	private static final long serialVersionUID = -8598343212986373989L;

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		Reservation reservation = reservation(req);
		if (repeat(req)) {
			for (Reservation r : repeated(req, reservation)) {
				getDao().persist(r);
			}
		}
		getDao().persist(reservation);
		return redirectToCalendar(req);
	}

	private List<Reservation> repeated(HttpServletRequest req, Reservation reservation) {
		return repeated(reservation, repeatInterval(req), repeatDuration(req));
	}

	private List<Reservation> repeated(Reservation base, int repeatInterval, int repeatDuration) {
		List<Reservation> reservations = new ArrayList<>();
		Reservation r = base;
		for (int i=1; i<repeatDuration; i++) {
			DateValue addedWeeks = WeekUtil.addWeeks(r.getDate(), repeatInterval);
			r = new Reservation(r, addedWeeks);
			reservations.add(r);
		}
		return reservations;
	}

	private int repeatDuration(HttpServletRequest req) {
		return Integer.valueOf(req.getParameter("repeatDuration"));
	}

	private int repeatInterval(HttpServletRequest req) {
		return Integer.valueOf(req.getParameter("repeatInterval"));
	}

	private boolean repeat(HttpServletRequest req) {
		if (!given(req.getParameter("repeat"))) return false;
		try {
			repeatDuration(req);
			repeatInterval(req);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private ResponseData redirectToCalendar(HttpServletRequest req) {
		URIBuilder b = new URIBuilder(getConfig().baseURL());
		b.addParameter("year", req.getParameter("year"));
		b.addParameter("weekId", req.getParameter("weekId"));
		return new ResponseData().setRedirectLocation(b.toString());
	}

	private Reservation reservation(HttpServletRequest req) {
		int spaceId = Integer.valueOf(req.getParameter("spaceId"));
		DateValue date = DateUtils.convertToDateValue(req.getParameter("date"));
		int startHour = Integer.valueOf(req.getParameter("startHour"));
		int endHour = Integer.valueOf(req.getParameter("endHour"));
		int startMinutes = Integer.valueOf(req.getParameter("startMinutes"));
		int endMinutes = Integer.valueOf(req.getParameter("endMinutes"));
		String personName = req.getParameter("personName");
		String eventName = req.getParameter("eventName");
		String eventDesc = req.getParameter("eventDesc");
		
		if ((startHour*100 + startMinutes) >= (endHour*100 + endMinutes)) throw new IllegalStateException("End before start");
		
		if (!given(eventName)) eventName = "UNKNOWN";
		if (!given(personName)) personName = "UNKNOWN";

		Reservation reservation = new Reservation(-1, spaceId, date, startHour, startMinutes, endHour, endMinutes, personName, eventName, eventDesc);
		return reservation;
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		log(req);
		int id = Integer.valueOf(getId(req));
		getDao().delete(id);
		return new ResponseData("ok", "text/plain").setOutputAlreadyPrinted();
	}

}
