package fi.luomus.spacereservation;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/login/*"})
public class LoginServlet extends SpaceReservationBaseServlet {

	private static final long serialVersionUID = 5784125919397871089L;

	@Override
	protected boolean authorized(HttpServletRequest req) {
		return true;
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return getLoginUtil().processPost(req, getSession(req));
	}

}
