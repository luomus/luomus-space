
CREATE TABLE space (
id      	NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
name    	VARCHAR2(500) NOT NULL,
name_short  VARCHAR2(500) NOT NULL
);

CREATE TABLE reservation (
id            NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
space_id      NUMBER NOT NULL,
year          NUMBER(4) NOT NULL,
week          VARCHAR2(10) NOT NULL,
month         NUMBER(2) NOT NULL,
day           NUMBER(2) NOT NULL,
start_hour    NUMBER(2) NOT NULL,
start_minutes NUMBER(2) NOT NULL,
end_hour      NUMBER(2) NOT NULL,
end_minutes   NUMBER(2)NOT NULL,
person_name   VARCHAR2(500) NOT NULL,
event_name    VARCHAR2(500) NOT NULL,
event_desc    VARCHAR2(2000 CHAR),
deleted       NUMBER(1),
CONSTRAINT fk_space
    FOREIGN KEY (space_id)
    REFERENCES space(id)
);
CREATE INDEX res_time ON reservation(year, week);


insert into space (name, name_short) values('Kaisaniemi: Sauna', 'Kais: Sauna');
insert into space (name, name_short) values('Kaisaniemi: Ulkopuutarha / Outdoor gardens', 'Kais: Ulkopuut.');
insert into space (name, name_short) values('Kaisaniemi: Elfving (14 hlöä)', 'Kais: Elfving');
insert into space (name, name_short) values('Kaisaniemi: Kasvihuoneet / Indoor gardens', 'Kais: Kasvih.');
insert into space (name, name_short) values('Kaisaniemi: Linkola (26 hlöä)', 'Kais: Linkola');
insert into space (name, name_short) values('Kaisaniemi: Työhuone 214 (8 hlöä; vapaa ti)', 'Kais: 214');
insert into space (name, name_short) values('Kaisaniemi: Nylander (50 hlöä)', 'Kais: Nylander');
insert into space (name, name_short) values('LTM: Kabinetti (15 hlöä)', 'LTM: Kabinetti');
insert into space (name, name_short) values('LTM: Aleksanteri (20 hlöä)', 'LTKM: Aleksanteri');
insert into space (name, name_short) values('LTM: Kimnaasi', 'LTKM: Kimnaasi');
insert into space (name, name_short) values('LTM: Työpaja / Workshop (vain opastuskäyttöön / only for guided tours)', 'LTM: Työpaja');
insert into space (name, name_short) values('LTM: Aula ja kahvila / Lobby and cafe', 'LTM: Aula+kahvila');
insert into space (name, name_short) values('LTM: Kahvila / Cafe (30 hlöä)', 'LTM: Kahvila');
insert into space (name, name_short) values('LTM: Koko talo / All spaces', 'LTM: Koko talo');
insert into space (name, name_short) values('Telttapaketti / Tent package', 'Telttapaketti');


CREATE TABLE openingtime (
id            NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
attraction_id NUMBER NOT NULL,
year          NUMBER(4) NOT NULL,
month         NUMBER(2) NOT NULL,
day           NUMBER(2) NOT NULL,
start_hour    NUMBER(2) NOT NULL,
start_minutes NUMBER(2) NOT NULL,
end_hour      NUMBER(2) NOT NULL,
end_minutes   NUMBER(2) NOT NULL,
CONSTRAINT ot_uniq UNIQUE (attraction_id, year, month, day),
CONSTRAINT ot_check_y CHECK (year BETWEEN 2022 AND 2100),
CONSTRAINT ot_check_m CHECK (month BETWEEN 1 AND 12),
CONSTRAINT ot_check_d CHECK (day BETWEEN 1 AND 31),
CONSTRAINT ot_check_sh CHECK (start_hour BETWEEN 1 AND 23),
CONSTRAINT ot_check_eh CHECK (end_hour BETWEEN 1 AND 23),
CONSTRAINT ot_check_sm CHECK (start_minutes BETWEEN 0 AND 59),
CONSTRAINT ot_check_em CHECK (end_minutes BETWEEN 0 AND 59)
);
CREATE INDEX opt_year ON openingtime(year);

