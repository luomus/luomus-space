<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
    <title>Luomus tilat | Space reservation</title>
	<link href="${staticURL}favicon.ico" type="image/ico" rel="shortcut icon" />
    <link href="${staticURL}bootstrap.min.css" rel="stylesheet">
    <link href="${staticURL}style.css?${staticContentTimestamp}" rel="stylesheet">
    <script src="${staticURL}/jquery-1.9.1.js"></script>
</head>

<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand uppercase" href="${baseURL}">LUOMUS</a>
        </div>
        <div class="pull-right" id="userinfo">
       		<ul>
       			<li><span class="hidden-sm name"><small>${username?html}</small></span></li>
       			<li><a href="${baseURL}/logout" class="btn btn-sm btn-primary" id="logout">Kirjaudu ulos | Logout</a></li>
       		</ul>
       	</div>
    </div>
</nav>
 
<div class="container">

	<#if roleError?has_content>
		<br />
		<div class="alert alert-danger">
			<p>${roleError}</p>
		</div>
	</#if>
	
	<h1>Luomus tilat | Space reservation</h1>
	
	<div class="form-group">	
		<label for="year">Vuosi | Year</label>  
		<input id="year" value="${year}" type="number" min="2020" max="2050" /> 
	
		&nbsp; &nbsp;
	 	
		<label for="weekId">Viikko | Week</label> 
		<select id="weekId">
			<#list weeks?values as w>
				<option value="${w.weekId}" <#if w.weekId == weekId>selected="selected" </#if> >${w.string}</option>
			</#list>
		</select>
		<#if nextWeekId??>
			&nbsp; <a href="${baseURL}?year=${year}&weekId=${nextWeekId}">Seuraava viikko / Next week &raquo;</a>
		</#if>
	</div>

</div>

	<div id="calendar">
		<table class="table table-striped">
			<thead class="thead-light">
				<tr>
					<#list ["Ma / Mon", "Ti / Tue", "Ke / Wed", "To / Thu", "Pe / Fri", "La / Sat", "Su / Sun"] as l>
						<th>${l}</th>
					</#list>
				</tr>
				<tr>
					<#list weekDays as day>
						<th <#if day.toEpochDay() == today>class="today"</#if>>${day.dayOfMonth}.${day.monthValue}</th>
					</#list>
				</tr>
			</thead>
			<tbody>
				<#list [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23] as hour>
					<tr>
						<#list weekDays as day>
							<td class="dayhour" data-date="${day.dayOfMonth}.${day.monthValue}.${year}" data-startHour="${hour}">
								<span class="hour">${hour}</span>
								<#list reservations.getReservations(day.dayOfMonth, day.monthValue, hour) as r>
									<div id="r_${r.id}_h${hour}" class="reservation reservation_${r_index}" style="background-color:${r.color}">
										<p class="title">${spaces[r.spaceId?string].nameShort} ${r.time}</p>
										<p class="name">${r.eventName?html}</p>
										<p class="person">${r.personName?html}</p>
									</div>
								</#list>
							</td>
						</#list>
					</tr>
				</#list>
			</tbody>
		</table>
	</div>

<div id="reservationAdd" style="display: none;">

	<form action="${baseURL}/reservation" method="POST" id="reservationAddForm">
	<input type="hidden" name="year" value="${year}" />
	<input type="hidden" name="weekId" value="${weekId}" />
	<input type="hidden" name="date" value="" id="reservationAddDateHidden" />
	
	<h1>Lisää varaus / Add a reservation</h1>
	
	<label for="reservationAddSpace">Tila / space</label>
	<select name="spaceId" id="reservationAddSpace" required>
		<option value="">Valitse tila / Select space</option>
		<#list spaces?values as s>
			<option value="${s.id}">${s.name}</option>
		</#list>
	</select> <br/>
	
	<label>Päivämäärä / Date</label> <span id="reservationAddDate"></span> <br/>
	
	<label for="reservationAddStartHour">Alkaa / Starts</label> 
	<input name="startHour" id="reservationAddStartHour" type="number" min="7" max="23" required style="width: 50px;" /> :
	<select name="startMinutes" id="reservationAddStartMinutes" required> <option value="0" selected="selected">00</option> <option value="15">15</option> <option value="30">30</option> <option value="45">45</option></select>
	<br />
	<label for="reservationAddEndHour">Loppuu / Ends</label>
	<input name="endHour" id="reservationAddEndHour" type="number" min="7" max="23" required  style="width: 50px;" /> :
	<select name="endMinutes" id="reservationAddEndMinutes" required> <option value="0">00</option> <option value="15">15</option> <option value="30">30</option> <option value="45">45</option></select>
	<br />
	
	<label for="reservationAddPersonName">Varaaja / By</label> <input name="personName" id="reservationAddPersonName" type="text" value="${username?html}" required pattern=".*\S.*" maxlength="50" /> <br />
	
	<label for="reservationAddEventName">Tapahtuma / Event</label> <input name="eventName" id="reservationAddEventName" required pattern=".*\S.*" maxlength="150"/> <br />
	
	<label for="reservationAddEventDesc">Kuvaus / Description</label> <br />
	<textarea name="eventDesc" id="reservationAddEventDesc" maxlength="1000"></textarea>
	
	<hr />
	
	<label>Toisto / Repeat</label>
	<input type="checkbox" name="repeat" id="repeatCheckbox" />
	<table id="repeatControls" style="display: none">
		<tr>
			<td>Toista</td>
			<td rowspan="2"><input type="number" value="1" min="1" max="8" name="repeatInterval" /></td>
			<td>viikon välein</td>
			<td rowspan="2"><input type="number" value="52" min="2" max="150" name="repeatDuration" /></td>
			<td>kertaa</td>
		</tr>
		<tr>
			<td>Repeat every</td>
			<td>weeks for </td>
			<td>times</td>
		</tr>
	</table>
	
	<hr />
	
	<button id="reservationAddSaveButton" type="submit" class="btn btn-primary">Tallenna / Save</button>
	<button id="reservationAddCloseButton" type="button" class="btn btn-secondary">Sulje / Close</button>
	</form>
</div>

<p>Tuki / Virheilmoitukset / Support / Error reports: <b>it-support@luomus.fi</b></p>


<script>
$(function() {

	function reload() {
		window.location.href = "?year="+$("#year").val() + "&weekId=" + $("#weekId").val();
	}
	
	$(".dayhour").on('mouseleave', function() {
		$(".addButton").remove();
	});
	
	$(".dayhour").on('mouseenter click', function() {
		var dayhour = $(this);
		$(".addButton").remove();
		var addButton = $('<button class="btn btn-sm btn-primary addButton">Varaa / Reserve</button>');
		addButton.on('click', function() {
			$("#reservationAddDate").text(dayhour.attr('data-date'));
			$("#reservationAddDateHidden").val(dayhour.attr('data-date'));
			$("#reservationAddStartHour").val(dayhour.attr('data-startHour'));
			$("#reservationAddStartMinutes").val("0");
			$("#reservationAddEndHour").val(parseInt(dayhour.attr('data-startHour'))+1);
			$("#reservationAddEndMinutes").val("0");
			$("#reservationAdd").show();
		});
		$(this).append(addButton);
	});
	
	$("#year").on('change', function() {
		window.location.href = "?year="+$("#year").val();
	});
	
	$("#weekId").on('change', function() {
		reload();
	});
	
	$("#reservationAddCloseButton").on('click', function() {
		if (confirm('Keskeytetäänkö varmasti? Really abort?')) {
			$("#reservationAdd").hide();
		}
	});
	
	$(".reservation").on('click', function() {
		$("#singleReservation").remove();
		var id = $(this).attr('id').replace('r_', '');
		id = id.substring(0, id.indexOf("_h"));
		var r = reservations[id];
		var display = $('<div id="singleReservation" style="background-color: '+r.color+'">');
		display.append($('<h1>'+r.space+'</h1>'));
		display.append($('<h2>'+r.time+'</h2>'));
		display.append($('<h3>'+r.person+'</h3>'));
		display.append($('<p>'+r.name+'</p>'));
		display.append($('<p>'+r.desc+'</p>'));
		display.append($('<hr/>'));
		var closeButton = $('<button type="button" class="btn btn-primary">Sulje / Close</button>');
		var deleteButton = $('<button style="float: right" type="button" class="btn btn-danger">Poista / Delete</button>');
		closeButton.on('click', function() {
			$("#singleReservation").remove();
		});
		deleteButton.on('click', function() {
			if (!confirm('Poistetaanko varmasti? / Really delete?')) return;
			$.ajax({
    			url: '${baseURL}/reservation/'+id,
		    	type: 'DELETE',
    			success: function(result) {
        			reload();
    			}
			});
		});
		display.append(closeButton);
        display.append(deleteButton);
		display.appendTo($("body"));
	});
	
	$("#reservationAddForm").on('submit', function() {
		var startHour = parseInt($('#reservationAddStartHour').val());
		var endHour = parseInt($('#reservationAddEndHour').val());
		var startMinutes = parseInt($('#reservationAddStartMinutes').val());
		var endMinutes = parseInt($('#reservationAddEndMinutes').val());
		if ((startHour*100 + startMinutes) >= (endHour*100 + endMinutes)) {
			alert('Alkuaika ei saa olla myöhemmin kuin loppuaika! Start must not be later than end time!');
			return false;
		}
		return true;
	});
	
	$("#repeatCheckbox").on('click', function() {
		$("#repeatControls").toggle(this.checked);
	});
});

var reservations = [];
<#list reservations.all as r>
	reservations[${r.id}] = { time: "${r.date.toString("d.M.yyyy")} ${r.time}", name: "${r.eventName?html}", desc: "${r.eventDesc?html}", person: "${r.personName?html}", space: "${spaces[r.spaceId?string].name}", color: "${r.color}" };
</#list>


	
</script>


</body>
</html>