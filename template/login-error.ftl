<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
    <title>Luomus tilat | space reservation</title>
	<link href="${staticURL}favicon.ico" type="image/ico" rel="shortcut icon" />
    <link href="${staticURL}bootstrap.min.css" rel="stylesheet">
    <link href="${staticURL}style.css?${staticContentTimestamp}" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand uppercase" href="http://luomus.fi">LUOMUS</a>
        </div>
    </div>
</nav>
 
<div class="container">
	<h1>Virhe / Error</h1>
	<pre class="alert alert-warning">${error}</pre>
</div>

</body>
</html>