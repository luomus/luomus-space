<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
    <title>Luomus aukioloajat | Opening times</title>
	<link href="${staticURL}favicon.ico" type="image/ico" rel="shortcut icon" />
    <link href="${staticURL}bootstrap.min.css" rel="stylesheet">
    <link href="${staticURL}style.css?${staticContentTimestamp}" rel="stylesheet">
    <script src="${staticURL}/jquery-1.9.1.js"></script>
</head>

<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand uppercase" href="${baseURL}/attractions">LUOMUS</a>
        </div>
        <div class="pull-right" id="userinfo">
       		<ul>
       			<li><span class="hidden-sm name"><small>${username?html}</small></span></li>
       			<li><a href="${baseURL}/logout" class="btn btn-sm btn-primary" id="logout">Kirjaudu ulos | Logout</a></li>
       		</ul>
       	</div>
    </div>
</nav>
 
<div class="container">

	<h1>Luomus aukioloajat | Opening times</h1>
	
	<div class="form-group">	
		<label for="year">Vuosi | Year</label>  
		<input id="year" value="${year}" type="number" min="2020" max="2050" /> 
	</div>

	<#if saveErrors?has_content>
		<br />
		<div class="alert alert-danger">
			<p>${saveErrors}</p>
		</div>
	</#if>
	
	<#if saveSuccess?has_content>
		<br />
		<div class="alert alert-success">
			<p>${saveSuccess}</p>
		</div>
	</#if>
	
<table id="openingtimes">
	<tr>
		<#list attractions as a>
			<th colspan="2">${a.name.forLocale(locale)}</th>
		</#list>
	</tr>
	<#list attractions as a>
		<td class="days">
			<#list days as day>
				<#if day.getDayOfWeek().toString() == "MONDAY">------<br /></#if>
				${day.dayOfMonth}.${day.monthValue}<br/>
			</#list>
		</td>
		<td>
			<form action="${baseURL}/attractions?year=${year}&attraction=${a.id}" method="POST">
			<@dataTextarea a />
			<p><input type="submit" class="button" value="Tallenna" /></p>
			</form>
		</td>
	</#list>
</table>


<h4>Käyttöohje</h4>

<pre>
------
11-19
11.30 - 19
suljettu
11-19.30
11.30 - 19.30
</pre>

<ol>
	<li> <code>------</code> = viikon vaihtumisen eroitin luettavuuden selkeyttämiseksi, älä koske</li>
	<li> <code>11 - 19</code> = Aukiolon voi ilmoittaa vain tunteina jolloin tulkitaan 11.00 - 19.00 </li>
	<li> <code>11.30 - 19</code> = Molemmissa tai toisessa ajassa voi olla minuutit erikseen, eroitin on piste </li>
	<li> <code>suljettu</code> = päivät joina kohde suljettu merkitään tekstillä "suljettu" </li>
	<li> <code>11-19.30</code> = Ajat voivat olla välilyöntejen kanssa tai ilman. </li>     
</ol>

<p>Aikoja voi esim muokata tekstieditorilla ja tuoda sitten tallennusarakkeeseen. Rivit erotellaan normaalilla rivivaihtomerkillä (enter).</p>
<p>Kun olet tallentanut kohteen ajat, näkymä päivittyy näyttämään tulkitut ajat. <b>Käy ne vielä läpi ja tarkista, että kaikki meni tietokantaan oikein!</b></p>
<p>Kannattaa syöttää ainakin alustavat ajat useille tuleville vuosille. Puuttuvat tiedot tulkitaan siten, että kohde on suljettu.</p>

<br /><br />
</div>
<p>Tuki / Virheilmoitukset / Support / Error reports: <b>it-support@luomus.fi</b></p>


<script>
$(function() {

	$("#year").on('change', function() {
		window.location.href = "?year="+$("#year").val();
	});
	
});
	
</script>

<#macro dataTextarea a>
	<#if prevData?? && prevAttraction == a.id>
<textarea name="data" rows="${daysCount}" cols="13" class="error">${prevData}</textarea>
	<#else>
<textarea name="data" rows="${daysCount}" cols="13"><#list days as day><#if day.getDayOfWeek().toString() == "MONDAY">------
</#if><#assign o = openingTimes.getOpeningTime(day, a.id)><#if o.isClosed()>suljettu<#else>${o.startHour}${minutes(o.startMinutes)} - ${o.endHour}${minutes(o.endMinutes)}</#if>
</#list></textarea>
	</#if>
</#macro>


<#function minutes i>
<#if i == 0> <#return ""></#if>
<#if i?string?length == 1> <#return ".0"+i></#if>
<#return "."+i>;
</#function>

</body>
</html>