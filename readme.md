Luomus space reservation calendar
======================================

## Installation

1. See /documentation/create_tables.sql
2. Place config file to <catalina.base>/app-con/luomus-space.properties

Config file example

~~~
SystemID = luomus-space
DevelopmentMode = YES
StagingMode = NO
ProductionMode= NO

SystemQname = KE.xxx # dev has KE that directs to localhost, production has KE that directs to production site
LajiAuthURL = https://..

BaseURL = http(s)://..
StaticURL = http(s)://..

BaseFolder = ...tomcat/
TemplateFolder = webapps/luomus-space/template
LanguageFileFolder = webapps/luomus-space/locale
LogFolder = application-logs/luomus-space
LanguageFilePrefix =
SupportedLanguages =

ErrorReporting_SMTP_Host = localhost
ErrorReporting_SMTP_Username =
ErrorReporting_SMTP_Password =
ErrorReporting_SMTP_SendTo = ...
ErrorReporting_SMTP_SendFrom = ...
ErrorReporting_SMTP_Subject = Luomus space reservation - Error Report

DBdriver = oracle.jdbc.OracleDriver
DBurl = jdbc:oracle:thin:@//...:1521/...
DBusername = 
DBpassword = 

 
~~~